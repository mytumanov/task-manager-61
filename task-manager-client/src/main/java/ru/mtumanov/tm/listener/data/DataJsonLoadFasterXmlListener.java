package ru.mtumanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataJsonLoadFasterXmlRq;
import ru.mtumanov.tm.dto.response.data.DataJsonLoadFasterXmlRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataJsonLoadFasterXmlListener extends AbstractDataListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-json";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataJsonLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadFasterXmlRs response = getDomainEndpoint().loadDataJsonFasterXml(new DataJsonLoadFasterXmlRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
