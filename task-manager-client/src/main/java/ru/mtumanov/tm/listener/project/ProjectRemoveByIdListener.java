package ru.mtumanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectRemoveByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectRemoveByIdRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIdListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRq request = new ProjectRemoveByIdRq(getToken(), id);
        @NotNull final ProjectRemoveByIdRs response = getProjectEndpoint().projectRemoveById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
