package ru.mtumanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class ApplicationExitListener extends AbstractSystemListener {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Close application";
    }

    @Override
    @NotNull
    public String getName() {
        return "exit";
    }

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.exit(0);
    }

}
