package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    int getSessionTimeout();

}
