package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoUserRepository;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.ExistLoginException;
import ru.mtumanov.tm.util.HashUtil;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserDtoService extends AbstractDtoService<UserDTO, IDtoUserRepository> implements IDtoUserService {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    private IDtoTaskRepository taskRepository;

    @NotNull
    @Autowired
    private IDtoProjectRepository projectRepository;

    @Override
    @NotNull
    @Transactional
    public UserDTO create(@NotNull final String login, @NotNull final String password) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        repository.save(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final String email) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        if (email.isEmpty())
            throw new EmailEmptyException();

        @NotNull final UserDTO user = create(login, password);
        user.setEmail(email);
        repository.save(user);
        return user;

    }

    @Override
    @NotNull
    @Transactional
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final Role role) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();


        @NotNull final UserDTO user = create(login, password);
        user.setRole(role);
        repository.save(user);
        return user;

    }

    @Override
    @NotNull
    public UserDTO findByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    @NotNull
    public UserDTO findByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        return repository.findByEmail(email);

    }

    @Override
    @NotNull
    public UserDTO findById(@NotNull final String id) throws AbstractException {
        if (id.isEmpty())
            throw new EmailEmptyException();
        Optional<UserDTO> user = repository.findById(id);
        if (!user.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + id);
        return user.get();
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO removeByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();

        @NotNull final UserDTO user = repository.findByLogin(login);
        taskRepository.deleteByUserId(user.getId());
        projectRepository.deleteByUserId(user.getId());
        repository.deleteById(user.getId());
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO removeByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();

        @NotNull final UserDTO user = repository.findByEmail(email);
        taskRepository.deleteByUserId(user.getId());
        projectRepository.deleteByUserId(user.getId());
        repository.deleteById(user.getId());
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO setPassword(@NotNull final String id, @NotNull final String password) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        Optional<UserDTO> user = repository.findById(id);
        if (!user.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + id);
        UserDTO userDTO = user.get();
        userDTO.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.save(userDTO);
        return userDTO;
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO userUpdate(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();

        Optional<UserDTO> user = repository.findById(id);
        if (!user.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + id);
        UserDTO userDTO = user.get();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setMiddleName(middleName);
        repository.save(userDTO);
        return userDTO;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty())
            return false;
        return repository.existsByLogin(login);

    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty())
            return false;
        return repository.existsByLogin(email);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();

        @NotNull final UserDTO user = repository.findByLogin(login);
        user.setLocked(true);
        repository.save(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();

        @NotNull final UserDTO user = repository.findByLogin(login);
        user.setLocked(false);
        repository.save(user);
    }

}
