package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoUserOwnedRepository;
import ru.mtumanov.tm.api.service.dto.IDtoUserOwnedService;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.mtumanov.tm.enumerated.EntitySort;
import ru.mtumanov.tm.exception.AbstractException;

import javax.transaction.Transactional;
import java.util.List;

public abstract class AbstractDtoUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IDtoUserOwnedRepository<M>>
        extends AbstractDtoService<M, R> implements IDtoUserOwnedService<M> {

    @Override
    @NotNull
    @Transactional
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        model.setUserId(userId);
        repository.save(model);
        return model;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final EntitySort sort) {
        if (sort == null)
            return repository.findAllByUserId(userId);
        return repository.findAllByUserId(userId, sort.getSort());
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return repository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

}
