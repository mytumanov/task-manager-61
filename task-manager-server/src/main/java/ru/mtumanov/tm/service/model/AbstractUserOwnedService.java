package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mtumanov.tm.api.repository.model.IUserOwnedRepository;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.api.service.model.IUserOwnedService;
import ru.mtumanov.tm.enumerated.EntitySort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;
import ru.mtumanov.tm.model.User;

import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    protected IUserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        @NotNull Optional<User> userOptional = getUserRepository().findById(userId);
        if (!userOptional.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + userId);
        @NotNull final User user = userOptional.get();
        model.setUser(user);
        repository.save(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final EntitySort sort) {
        if (sort == null)
            return repository.findAllByUserId(userId);
        return repository.findAllByUserId(userId, sort.getSort());
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return repository.countByUserId(userId);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        repository.delete(model);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

}
