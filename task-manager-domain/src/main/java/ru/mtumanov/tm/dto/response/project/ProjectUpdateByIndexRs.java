package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectUpdateByIndexRs extends AbstractProjectRs {

    public ProjectUpdateByIndexRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectUpdateByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}