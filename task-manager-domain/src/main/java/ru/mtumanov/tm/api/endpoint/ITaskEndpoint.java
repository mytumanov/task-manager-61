package ru.mtumanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.*;
import ru.mtumanov.tm.dto.response.task.*;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(final String host, final String port) {
        return IEndpoint.newInstance(host, port, NAME, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskChangeStatusByIdRs taskChangeStatusById(@NotNull TaskChangeStatusByIdRq request);

    @NotNull
    @WebMethod
    TaskClearRs taskClear(@NotNull TaskClearRq request);

    @NotNull
    @WebMethod
    TaskCompleteByIdRs taskCompleteById(@NotNull TaskCompleteByIdRq request);

    @NotNull
    @WebMethod
    TaskCreateRs taskCreate(@NotNull TaskCreateRq request);

    @NotNull
    @WebMethod
    TaskListRs taskList(@NotNull TaskListRq request);

    @NotNull
    @WebMethod
    TaskRemoveByIdRs taskRemoveById(@NotNull TaskRemoveByIdRq request);

    @NotNull
    @WebMethod
    TaskShowByIdRs taskShowById(@NotNull TaskShowByIdRq request);

    @NotNull
    @WebMethod
    TaskShowByProjectIdRs taskShowByProjectId(@NotNull TaskShowByProjectIdRq request);

    @NotNull
    @WebMethod
    TaskStartByIdRs taskStartById(@NotNull TaskStartByIdRq request);

    @NotNull
    @WebMethod
    TaskUpdateByIdRs taskUpdateById(@NotNull TaskUpdateByIdRq request);

    @NotNull
    @WebMethod
    TaskBindToProjectRs taskBindToProject(@NotNull TaskBindToProjectRq request);

    @NotNull
    @WebMethod
    TaskUnbindFromProjectRs taskUnbindFromProject(@NotNull TaskUnbindFromProjectRq request);

}
